This is the standard installation procedure for installing opencv-3.1.0 on Ubuntu (tested on 14.04.5 Trust Tahr)
In order to execute just clone the repository....

$ cd opencv_ubuntu_installation

$chmod +x install_opencv_linux.sh

$./install_opencv_linux.sh

This will install all the required packages and libraries for opencv. All the packages required or video processing
will also be installed.

After the installation is complete, try importing opencv in python.

$python
..
..
>>> import cv2
>>>